package com.ben.sam.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.ben.sam.consts.DB;
import com.ben.sam.dao.ICustomerDao;
import com.ben.sam.po.Customer;

@Component("customerDao")
public class CustomerDaoImpl implements ICustomerDao{

	public Customer persist(Customer c) {
		// TODO Auto-generated method stub
		System.out.println("customer persist.....");
		return DB.find(c.getCustomerId());
	}

	public List<Customer> list() {
		// TODO Auto-generated method stub
		return DB.findAll();
	}

	public Customer list(String customerId) {
		// TODO Auto-generated method stub
		return DB.find(customerId);
	}

	public Customer update(Customer customer) {
		// TODO Auto-generated method stub
		return DB.update(customer);
	}

	@Override
	public Customer delete(String customerId) {
		// TODO Auto-generated method stub
		Customer c = new Customer();
		DB.delete(customerId);
		c.setCustomerId(customerId);
		return c;
	}

}
