package com.ben.sam.po;

public class Customer {
	private String customerId;
	private String customerName;
	private String password;
	private String customerLocation;
	
	public Customer() {}
	
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getCustomerLocation() {
		return customerLocation;
	}
	public void setCustomerLocation(String customerLocation) {
		this.customerLocation = customerLocation;
	}
	public Customer(String customerId, String customerName, String password, String customerLocation) {
		super();
		this.customerId = customerId;
		this.customerName = customerName;
		this.password = password;
		this.customerLocation = customerLocation;
	}
	
}
