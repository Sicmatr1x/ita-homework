package com.ben.sam.controller;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ben.sam.po.Customer;
import com.ben.sam.service.ExampleService;
import com.ben.sam.service.ICustomerService;
import com.google.gson.JsonObject;
import com.google.gson.Gson;

@RestController
@RequestMapping("/v1")
public class ExampleController {
	@Resource(name = "eService")
	private ICustomerService service;
	Customer customer = new Customer("007","chan","123456","ZHA");
	List<Customer> customers = new ArrayList<Customer>();
	Gson gson = new Gson();

	@PostMapping("/customer")
	public String register(@RequestParam(value="id") String id,@RequestParam(value="password") String password) {
		Customer customer = new Customer();
		customer.setCustomerId(id);
		customer.setPassword(password);
		customer = service.register(customer);
		String json = gson.toJson(customer);
		return json;
	}
	
	@GetMapping("/customer")
	public String login(@RequestParam(value="id") String id,@RequestParam(value="password") String password){
		Customer customer  = service.login(id, password);
		String json = gson.toJson(customer);
		return json;
	}
	
	@PutMapping("/customer")
	public String update(
			@RequestParam(value="id") String id,
			@RequestParam(value="name") String name,
			@RequestParam(value="password") String password,
			@RequestParam(value="location") String location) {
		Customer customer = new Customer(id,name,password,location);
		service.update(customer);
		String json = gson.toJson(customer);
		return json;
	}
	
	@DeleteMapping("/customer")
	public String delete(@RequestParam(value="id") String id) {
		Customer customer = service.delete(id);
		String json = gson.toJson(customer);
		return json;
	}
	
	@GetMapping("/customers")
	public String listAll() {
		List<Customer> list = service.showAll();
//		customers.add(customer);
//		customers.add(customer);
		String json = gson.toJson(list);
		return json;
	}
	
}
