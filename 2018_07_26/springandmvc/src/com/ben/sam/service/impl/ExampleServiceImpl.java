package com.ben.sam.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.ben.sam.dao.ExampleDao;
import com.ben.sam.service.ExampleService;

@Service("eService")
public class ExampleServiceImpl implements ExampleService {
	@Resource(name="eDao")
	private ExampleDao dao;

	@Override
	public void example() {
		// TODO Auto-generated method stub
		dao.example();
	}

}
