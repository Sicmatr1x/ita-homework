package com.ben.sam.service;

import java.util.List;

import com.ben.sam.po.Customer;

public interface ICustomerService {
	Customer login(String customerName,String password);
	Customer register(Customer c);
	List<Customer> showAll();
	Customer showOne(String customerId);
	Customer update(Customer c);
	Customer delete(String customerId);
}
