package com.ben.sam.aop;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class HelloAspect {
	@Before("execution (public * register (..))")
	public void before() {
		System.out.println("before....");
	}
	
//	@Before("pointcut()")
//	public void pointcut() {
//		System.out.println("point");
//	}
//	
//	@After("pointcut()")
//	public void after() {
//		System.out.println("after");
//	}
//	
//	@Around("around()")
//	public void around() throws Throwable{
//		System.out.println("AROUND...");
//	}

}
