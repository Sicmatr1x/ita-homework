package com.ben.sam.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.ben.sam.dao.ICustomerDao;
import com.ben.sam.po.Customer;
import com.ben.sam.service.ICustomerService;

@Service("customerSerivce")
public class CustomerServiceImpl implements ICustomerService {
	@Resource(name="customerDao")
	private ICustomerDao dao;
	
	public Customer login(String customerName, String password) {
		// TODO Auto-generated method stub
		System.out.println("customer login....."+customerName+","+password);
		Customer c = new Customer();
		c.setCustomerId(customerName);
		c = dao.persist(c);
		if(c.getCustomerId()==customerName&&c.getPassword()==password) {
			return c;
		}
		return null;
	}

	public Customer register(Customer c) {
		// TODO Auto-generated method stub
		Customer res = dao.persist(c);
		if(res!=null) {
			return null;
		}else {
			dao.persist(c);
		}
		return res;
	}

	public List<Customer> showAll() {
		// TODO Auto-generated method stub
		return dao.list();
	}

	public Customer showOne(String customerId) {
		// TODO Auto-generated method stub
		return dao.list(customerId);
	}

	public Customer update(Customer c) {
		// TODO Auto-generated method stub
		return dao.update(c);
	}

	public Customer delete(String customerId) {
		// TODO Auto-generated method stub
		return dao.delete(customerId);
	}

}
