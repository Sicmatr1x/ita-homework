package com.ben.sam.aop;

import org.apache.commons.codec.digest.DigestUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class LoginAspect {
//	@Around("execution (public * login(..))")
	@Around("execution (* com.ben.sam.service.impl.CustomerServiceImpl.login (..))")
	public Object[] loginAround(ProceedingJoinPoint pjp) {
		Object[] args = pjp.getArgs();
		String pwd = (String) args[1];
		args[1] = DigestUtils.md5Hex(pwd);
		try {
			pjp.proceed(args);
		} catch (Throwable e) {
			e.printStackTrace();
		}
		return args;
	}
	
//	@Around("execution (public * register(..))")
	@Around("execution (* com.ben.sam.service.impl.CustomerServiceImpl.register (..))")
	public Object[] registerAround(ProceedingJoinPoint pjp) {
		Object[] args = pjp.getArgs();
		String pwd = (String) args[1];
		args[1] = DigestUtils.md5Hex(pwd);
		try {
			pjp.proceed(args);
		} catch (Throwable e) {
			e.printStackTrace();
		}
		return args;
	}
}
