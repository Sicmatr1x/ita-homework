package com.ben.sam.consts;

import java.security.KeyStore.Entry;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.text.html.parser.Entity;

import com.ben.sam.po.Customer;

public class DB {
	private static Map<String, Customer> customers = new HashMap<String, Customer>();

	static{
		for(int i=0;i<8;i++) {
			String id="00"+i;
			String name="xiaoming_"+id;
			String password="pwd"+id;
			String location="street "+(char)(Integer.parseInt(id)+96);
			Customer customer = new Customer( id, name,password, location);
			customers.put(id,customer);
		}
	}

	public static void add(Customer customer) {
		customers.put(customer.getCustomerId(), customer);
	}

	public static void delete(String customerId) {
		customers.remove(customerId);
	}

	public static Customer update(Customer customer) {
		String id = customer.getCustomerId();
		customers.remove(id);
		customers.put(id, customer);
		return customer;
	}

	public static Customer find(String customerId) {
		return customers.get(customerId);
	}

	public static List<Customer> findAll() {
		List<Customer> customerlist = new ArrayList<Customer>();
		for (Map.Entry key : customers.entrySet()) {
			customerlist.add((Customer) key.getValue());
		}
		return customerlist;
	}

}
