package com.ben.sam.dao;

import java.util.List;

import com.ben.sam.po.Customer;

public interface ICustomerDao {
	Customer persist(Customer c);
	List<Customer> list();
	Customer list(String customerId);
	Customer update(Customer customer);
	Customer delete(String customerId);
}
